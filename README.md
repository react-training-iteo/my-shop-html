# React Szkolenie - My-shop - HTML

<img src="./my-shop.png" style="border: 1px solid #bbb">

### HTML
```HTML
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>My Shop</title>
    </head>
    <body cz-shortcut-listen="true">
    <div id="root">
      <div class="App">
        <header class="header-app">
          <img class="header-app--logo" src="logo.png" alt="Logo"> My-shop application
        </header>
        <div class="container">
          <section class="product-list">
            <div class="search"><input class="search-input" placeholder="Szukaj..."></div>
            <div class="product-box">
              <div><img class="product-box--image" src="https://placeimg.com/140/180/people"></div>
              <div><h2>Sony Play Station 4</h2>
                <pre>Cena: 1399zł</pre>
                <p>Poznaj bardziej smukłe, mniejsze PS4, które oferuje graczom niesamowite wrażenia z gier.</p>
                <button>Dodaj do koszyka</button>
              </div>
            </div>
            <div class="product-box">
              <div><img class="product-box--image" src="https://placeimg.com/140/180/nature"></div>
              <div><h2>Xbox One X</h2>
                <pre>Cena: 1500zł</pre>
                <p>Na Xbox One X gra się jeszcze lepiej. Dzięki większej o 40% mocy niż na jakiejkolwiek innej konsoli.</p>
                <button>Dodaj do koszyka</button>
              </div>
            </div>
            <div class="product-box">
              <div><img class="product-box--image" src="https://placeimg.com/140/180/dog"></div>
              <div><h2>Macbook PRO 2018</h2>
                <pre>Cena: 14900zł</pre>
                <p>MacBook Pro - dotyk czyni cuda Jest niewiarygodnie smukły, lekki jak piórko, a do tego potężniejszy i
                  szybszy niż kiedykolwiek</p>
                <button>Dodaj do koszyka</button>
              </div>
            </div>
            <div class="product-box">
              <div><img class="product-box--image" src="https://placeimg.com/140/180/architecture"></div>
              <div><h2>Macbook Air 2018</h2>
                <pre>Cena: 3600zł</pre>
                <p>11-calowy MacBook Air działa bez ładowania baterii do 9 godzin, a 13-calowy - nawet 12.</p>
                <button>Dodaj do koszyka</button>
              </div>
            </div>
            <div class="product-box">
              <div><img class="product-box--image" src="https://placeimg.com/140/180/tech"></div>
              <div><h2>iPhone X</h2>
                <pre>Cena: 7299zł</pre>
                <p>iPhone X to telefon, który nie tyle posiada ekran, ile po prostu sam tym ekranem jest.</p>
                <button disabled="">Dodaj do koszyka</button>
              </div>
            </div>
          </section>
          <section class="user-cart"><h4>Twój koszyk</h4>
            <ul class="user-cart--list">
              <li>
                <button>x</button>
                0 - Sony Play Station 4 - 1399 zł
              </li>
              <li>
                <button>x</button>
                1 - Xbox One X - 1500 zł
              </li>
              <li>
                <button>x</button>
                2 - Macbook PRO 2018 - 14900 zł
              </li>
            </ul>
            <section class="total-price">Podsumowanie: 17799 zł</section>
          </section>
        </div>
        <footer class="footer">Copyright by my-shop - 2018</footer>
      </div>
    </div>
    </body>
  </html>
```

### CSS
```CSS
  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    width: 1170px;
    margin: 0 auto;
  }

  .container {
    position: relative;
    display: -ms-flexbox;
    display: flex;
  }

  .header-app {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    margin-bottom: 20px;
    padding: 20px 0;
    border-bottom: 1px solid #ddd;
  }

  .header-app--logo {
    width: 30px;
    margin-right: 10px;
    height: 34px;
  }

  .footer {
    width: 100%;
    font-size: 11px;
    text-align: center;
    margin-top: 20px;
    padding: 20px 0;
    border-top: 1px solid #ddd;
  }

  .product-list {
    width: 770px;
    margin: 20px 0;
    border-right: 1px solid #ddd;
  }

  .search {
    width: 750px;
    padding: 0 0 20px;
    margin-right: 20px;
    margin-bottom: 20px;
    border-bottom: 1px solid #ddd;
  }

  .search-input {
    width: 350px;
    padding: 6px;
  }

  .product-box {
    display: -ms-flexbox;
    display: flex;
    margin: 20px 20px 0 0;
  }

  .product-box--image {
    margin-right: 20px;
  }

  .header-app {
    margin-bottom: 20px;
    padding: 20px 0;
    border-bottom: 1px solid #ddd;
  }

  .user-cart--list {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  .user-cart--list li {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
  }

  .user-cart--list button {
    text-align: center;
    margin-right: 15px;
    border-radius: 50%;
    width: 22px;
    height: 22px;
  }

  .user-cart {
    position: relative;
    width: 300px;
    margin-left: 20px;
  }

  .total-price {
    width: 100%;
    margin-top: 20px;
    padding-top: 20px;
    border-top: 1px solid #ddd;
  }
```

Produkty

```JSON
  [
    {
        "id": 1,
        "name": "Sony Play Station 4",
        "description": "Poznaj bardziej smukłe, mniejsze PS4, które oferuje graczom niesamowite wrażenia z gier.",
        "photo": "https://placeimg.com/140/180/people",
        "price": 1399,
        "in_stock": true
    },
    {
        "id": 2,
        "name": "Xbox One X",
        "description": "Na Xbox One X gra się jeszcze lepiej. Dzięki większej o 40% mocy niż na jakiejkolwiek innej konsoli.",
        "photo": "https://placeimg.com/140/180/nature",
        "price": 1500,
        "in_stock": true
    },
    {
        "id": 3,
        "name": "Macbook PRO 2018",
        "description": "MacBook Pro - dotyk czyni cuda Jest niewiarygodnie smukły, lekki jak piórko, a do tego potężniejszy i szybszy niż kiedykolwiek",
        "photo": "https://placeimg.com/140/180/dog",
        "price": 14900,
        "in_stock": true
    },
    {
        "id": 4,
        "name": "Macbook Air 2018",
        "description": "11-calowy MacBook Air działa bez ładowania baterii do 9 godzin, a 13-calowy - nawet 12.",
        "photo": "https://placeimg.com/140/180/architecture",
        "price": 3600,
        "in_stock": true
    },
    {
    "id": 5,
    "name": "iPhone X",
    "description": "iPhone X to telefon, który nie tyle posiada ekran, ile po prostu sam tym ekranem jest.",
    "photo": "https://placeimg.com/140/180/tech",
    "price": 7299,
    "in_stock": false
    }
  ]
```